


import torch
from torch import nn
# from transformers import BertModel
from modeling_wwm import BertModel





class PretrainedModel(nn.Module):
    def __init__(self,config):
        super().__init__()
        self.bert = BertModel.from_pretrained(config.bert_model_dir)
        self.tnews_classifier = nn.Linear(config.hidden_size,config.tnews_label_num,bias=True)
        self.ocemotion_classifier = nn.Linear(config.hidden_size,config.ocemotion_label_num,bias=True)
        self.ocnli_classifier = nn.Linear(config.hidden_size,config.ocnli_label_num,bias=True)
        self.dropout = nn.Dropout(0.1)
        self.softmax = nn.Softmax()
        self.tnews_label_num = config.tnews_label_num
        self.ocemotion_label_num = config.ocemotion_label_num
        self.ocnli_label_num = config.ocnli_label_num


    def forward(self,input_ids,token_type_ids,attention_mask,global_step,labels=None):
        sequence_output,pooled_output = self.bert(input_ids,token_type_ids=token_type_ids,attention_mask=attention_mask)
        output = None
        if global_step%3 == 0:
            output = self.tnews_classifier(pooled_output)
        elif global_step%3 == 2:
            output = self.ocemotion_classifier(pooled_output)
        else:
            output = self.ocnli_classifier(pooled_output)
        output = self.dropout(output)
        logits = self.softmax(output)
        outputs = (logits,)
        if labels is not None:
            loss_fun = nn.CrossEntropyLoss()
            if global_step%3==0:
                loss = loss_fun(output.view(-1,self.tnews_label_num),labels.view(-1))
            elif global_step%3==2:
                loss = loss_fun(output.view(-1,self.ocemotion_label_num),labels.view(-1))
            else:
                loss = loss_fun(output.view(-1,self.ocnli_label_num),labels.view(-1))
            outputs = outputs + (loss,)
        return outputs


