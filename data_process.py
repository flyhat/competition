
import argparse
from transformers import BertTokenizer
import pandas as pd
import torch
from torch.utils.data import TensorDataset
import logging
import random
from torch.utils.data import SequentialSampler,DataLoader,RandomSampler
from itertools import cycle


class Example:
    def __init__(self,idx,sent_one,sent_two=None,label=None):
        self.idx = idx
        self.sent_one = sent_one
        self.sent_two = sent_two
        self.label = label

class Feature:
    def __init__(self,idx,input_ids,token_type_ids,attention_mask,input_len,label=None):
        self.idx = idx
        self.input_ids = input_ids
        self.token_type_ids = token_type_ids
        self.attention_mask = attention_mask
        self.input_len = input_len
        self.label = label


def read_csv_data(filepath,sent_two = False,is_test=False):
    if not sent_two:
        dataset = pd.read_csv(filepath,sep='\t',names=['idx','sentence','label'])
    else:
        dataset = pd.read_csv(filepath,sep='\t',names=['idx','sent_one','sent_two','label'])
    if is_test:
        dataset['label'] = None
    return dataset

def get_data_label(dataset):
    label_set = dataset['label'].unique()
    label2idx = {l:idx for idx,l in enumerate(label_set)}
    idx2label = {idx:l for idx,l in enumerate(label_set)}
    return label2idx,idx2label

def split_data_to_train_and_dev(dataset):
    random.shuffle(dataset)
    data_len = len(dataset)
    train_data = dataset[:int(data_len*0.9)]
    print('the train data size is %s'%(len(train_data)))
    dev_data = dataset[int(data_len*0.9):]
    print('the dev data size is %s'%(len(dev_data)))
    return train_data,dev_data

def convert_data_to_example(dataset,sent_two=False,is_test=False):
    examples = []
    if not is_test:
        if sent_two:
            for data in dataset:
                idx,sent_one,sent_two,label = data[0],data[1],data[2],data[3]
                examples.append(Example(idx=idx,sent_one=sent_one,sent_two=sent_two,label=label))
        else:
            for data in dataset:
                idx,sent_one,label = data[0],data[1],data[2]
                examples.append(Example(idx=idx,sent_one=sent_one,label=label))
    else:
        if sent_two:
            for data in dataset:
                idx,sent_one,sent_two = data[0],data[1],data[2]
                examples.append(Example(idx=idx,sent_one=sent_one,sent_two=sent_two))
        else:
            for data in dataset:
                idx,sent_one = data[0],data[1]
                examples.append(Example(idx=idx,sent_one=sent_one))
    return examples


def convert_example_to_feature(examples,tokenizer,max_length,pad_token=0):
    features = []
    for example in examples:
        input_dict = tokenizer.encode_plus(example.sent_one,example.sent_two,add_special_tokens=True,max_length=max_length,truncation=True)
        input_ids,token_type_ids = input_dict['input_ids'],input_dict['token_type_ids']
        input_ids = input_ids
        token_type_ids = token_type_ids
        input_len = len(input_ids)
        attention_mask = [1] * input_len
        if len(input_ids) <= max_length:
            padding_length = max_length - len(input_ids)
            input_ids = input_ids + ([pad_token] * padding_length)
            attention_mask = attention_mask + ([0] * padding_length)
            token_type_ids = token_type_ids + ([0] * padding_length)
        else:
            input_ids = input_ids[:max_length]
            attention_mask = attention_mask[:max_length]
            token_type_ids = token_type_ids[:max_length]
        assert len(input_ids) == max_length,"Error with input length {} vs {}".format(len(input_ids), max_length)
        assert len(token_type_ids) == max_length,"Error with input length {} vs {}".format(len(token_type_ids), max_length)
        assert len(attention_mask) == max_length,"Error with input length {} vs {}".format(len(attention_mask), max_length)
        features.append(
            Feature(idx=example.idx,
                    input_ids=input_ids,
                    token_type_ids=token_type_ids,
                    attention_mask = attention_mask,
                    input_len=input_len,
                    label=example.label
                    )
        )
    return features

def trans_data(filepath,tokenizer,max_length,is_two_sent=False,is_test=False):
    dataset = read_csv_data(filepath,is_two_sent,is_test)
    label2idx,idx2label = None,None
    if not is_test:
        label2idx,idx2label = get_data_label(dataset)
        dataset['label'] = dataset['label'].map(label2idx)
    dataset = dataset.values
    data_example = convert_data_to_example(dataset,sent_two=is_two_sent,is_test=is_test)
    data_feature = convert_example_to_feature(data_example,tokenizer,max_length)

    return data_feature,idx2label

def get_train_dev_data(tnews_path,ocnli_path,ocemotion_path,tokenizer,max_length):
    tnews_dataset,tnews_idx2label = trans_data(tnews_path,tokenizer,max_length)
    ocemotion_dataset,ocemotion_idx2label = trans_data(ocemotion_path,tokenizer,max_length)
    ocnli_dataset,ocnli_idx2label = trans_data(ocnli_path,tokenizer,max_length,is_two_sent=True)

    tnews_features,tnews_dev_features = split_data_to_train_and_dev(tnews_dataset)
    ocemotion_features,ocemotion_dev_features = split_data_to_train_and_dev(ocemotion_dataset)
    ocnli_features,ocnli_dev_features = split_data_to_train_and_dev(ocnli_dataset)

    return tnews_features,tnews_idx2label,tnews_dev_features,ocemotion_features,ocemotion_idx2label,ocemotion_dev_features,ocnli_features,ocnli_idx2label,ocnli_dev_features


def convert_feature_to_tensor(features,is_test=False):
    all_input_ids = torch.tensor([f.input_ids for f in features],dtype=torch.long)
    all_attention_mask = torch.tensor([f.attention_mask for f in features],dtype=torch.long)
    all_token_type_ids = torch.tensor([f.token_type_ids for f in features],dtype=torch.long)
    all_len = torch.tensor([f.input_len for f in features],dtype=torch.long)
    all_idx = torch.tensor([f.idx for f in features],dtype=torch.long)
    if not is_test:
        all_label = torch.tensor([f.label for f in features],dtype=torch.long)
        dataset = TensorDataset(all_idx,all_len,all_input_ids,all_attention_mask,all_token_type_ids,all_label)
        return dataset
    else:
        dataset = TensorDataset(all_idx,all_len,all_input_ids,all_attention_mask,all_token_type_ids)
        return dataset

def train_collate_fn(batch):
    all_idx,all_len,all_input_ids,all_attention_mask,all_token_type_ids,all_labels = map(torch.stack,zip(*batch))
    max_len = max(all_len).item()
    all_input_ids = all_input_ids[:,:max_len]
    all_attention_mask = all_attention_mask[:,:max_len]
    all_token_type_ids = all_token_type_ids[:,:max_len]
    return all_input_ids,all_attention_mask,all_token_type_ids,all_labels,all_idx


def test_collate_fn(batch):
    all_idx,all_len,all_input_ids,all_attention_mask,all_token_type_ids = map(torch.stack,zip(*batch))
    max_len = max(all_len).item()
    all_input_ids = all_input_ids[:,:max_len]
    all_attention_mask = all_attention_mask[:,:max_len]
    all_token_type_ids = all_token_type_ids[:,:max_len]
    return all_input_ids,all_attention_mask,all_token_type_ids,all_idx




if __name__=='__main__':
    ocemotion_file,tnews_file,ocnli_file = './data/OCEMOTION_all.csv','./data/TNEWS_all.csv','./data/OCNLI_all.csv'
    tokenizer = BertTokenizer.from_pretrained('./bert/')
    max_len = 128
    tnews_features,tnews_idx2label,ocemotion_features,ocemotion_idx2label,ocnli_features,ocnli_idx2label = get_all_data(tnews_file,ocnli_file,ocemotion_file,tokenizer,max_len)



