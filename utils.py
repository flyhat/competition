

import json
import os
import torch
import shutil
from sklearn.metrics import classification_report


class RunningAverage():

    def __init__(self):
        self.steps = 0
        self.total = 0
    def upgrade(self,val):
        self.total += val
        self.steps += 1
    def __call__(self):
        return self.total / float(self.steps)


def write_output_data(output_path,result):
    with open(output_path,'w',encoding='utf8') as f:
        for idx,predict in zip(result['idx'],result['predict']):
            tmp = {'id':str(idx),'label':str(predict) if not isinstance(predict,str) else predict}
            tmp = json.dumps(tmp)
            f.write(tmp + '\n')


def write_classification_report(data,target_name,output_path):
    with open(output_path,'w',encoding='utf8') as f:
        data_report = classification_report(data['label'],data['predict'],target_names=target_name,output_dict=True)
        json.dump(data_report,f)


def save_checkpoint(state,checkpoint,kf=None):
    filepath = os.path.join(checkpoint,'best_%s.pth.tar'%(state['epoch']))
    if not os.path.exists(checkpoint):
        #print("Checkpoint Directory does not exist! Making directory {}".format(checkpoint))
        os.mkdir(checkpoint)

    torch.save(state,filepath)


def load_checkpoint(checkpoint,model,optimizer=None):
    if not os.path.exists(checkpoint):
        raise("File dosen't exist {}".format(checkpoint))
    checkpoint = torch.load(checkpoint)
    model.load_state_dict(checkpoint['state_dict'])
    if optimizer:
        optimizer.load_state_dict(checkpoiont['optim_dict'])
    return checkpoint


if __name__=='__main__':
    target_name = ['1','2']
    data = {'label':[0,1,0,1,0,1,0,1,1,1],'predict':[1,0,1,0,0,1,0,1,1,1]}
    path = './result_output/tesy.json'
    write_classification_report(data,target_name,path)


