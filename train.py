


import argparse
import random
import os
import torch
from torch.optim.lr_scheduler import LambdaLR

from model import PretrainedModel
from tqdm import tqdm
from tqdm import trange
from torch import nn
from data_process import train_collate_fn,test_collate_fn,get_train_dev_data,convert_feature_to_tensor,trans_data
from transformers import AdamW,BertTokenizer,BertConfig
from sklearn.metrics import classification_report,confusion_matrix,f1_score
from torch.utils.data import SequentialSampler,DataLoader,RandomSampler
from optimization import BertAdam, warmup_linear
from itertools import cycle
import json
import logging
import numpy as np
from utils import RunningAverage,write_output_data,save_checkpoint,write_classification_report,load_checkpoint

parser = argparse.ArgumentParser()
parser.add_argument('--model_dir', default='./model_output', help="Directory containing params.json")
parser.add_argument('--data_dir', default='./data', help="Directory containing the dataset")
parser.add_argument('--test_output',default='./submition', help="Directory containing the dataset")
parser.add_argument('--result_output', default='./result_output', help="Directory containing the dataset")
parser.add_argument('--bert_model_dir', default='./bert', help="Directory containing the BERT model in PyTorch")
parser.add_argument('--seed', type=int, default=42, help="random seed for initialization")
parser.add_argument('--restore_file', default=None,
                    help="Optional, name of the file in --model_dir containing weights to reload before training")


parser.add_argument('--fp16', default=False, action='store_true', help="Whether to use 16-bit float precision instead of 32-bit")
parser.add_argument('--loss_scale', type=float, default=0,
                        help="Loss scaling to improve fp16 numeric stability. Only used when fp16 set to True.\n"
                             "0 (default value): dynamic loss scaling.\n"
                             "Positive power of 2: static loss scaling value.\n")
parser.add_argument('--use_docker',type=bool,default=False)
parser.add_argument('--multi_gpu',type=bool,default=True,help="Whether to use multiple GPUs if available")
parser.add_argument('--device_id',type = str,default='0 1 2')
parser.add_argument('--bert_output_dim',type=int,default=1024)

parser.add_argument('--label_len',type = int,default=1)
parser.add_argument('--is_one_sent',type=bool,default=False)
parser.add_argument('--model_type',type=str,default='SentModel')
parser.add_argument('--max_len',type=int,default='128')
parser.add_argument('--batch_size',type=int,default='24')
parser.add_argument('--learning_rate',type=float,default='2e-5')
parser.add_argument('--use_cnn',type=bool,default=True)
parser.add_argument('--is_concat',type=bool,default=True)
parser.add_argument('--is_training',type=bool,default=True)
parser.add_argument('--is_eval',type=bool,default=True)
parser.add_argument('--device',type=str,default='cuda')
parser.add_argument('--epoches',type=int,default=20)
parser.add_argument('--hidden_size',type=int,default='1024')
parser.add_argument('--tnews_predict_path',type=str,default='tnews_predict.json')
parser.add_argument('--ocemotion_predict_path',type=str,default='ocemotion_predict.json')
parser.add_argument('--ocnli_predict_path',type=str,default='ocnli_predict.json')
parser.add_argument('--tnews_report_path',type=str,default='tnews_report.json')
parser.add_argument('--ocemotion_report_path',type=str,default='ocemotion_report.json')
parser.add_argument('--ocnli_report_path',type=str,default='ocnli_report.json')
parser.add_argument('--load_checkpoint',type=bool,default=False)
parser.add_argument('--checkpoint_path',type=str,default='./model_output/best.pth')



def seed_everything(seed=42):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    torch.backends.cudnn.deterministic=True

def write_result(tnews_result,ocemotion_result,ocnli_result,tnews_target_name,ocemotion_target_name,ocnli_target_name,args):
    tnews_report_output = os.path.join(args.result_output,args.tnews_report_path)
    ocemotion_report_output = os.path.join(args.result_output,args.ocemotion_report_path)
    ocnli_report_output = os.path.join(args.result_output,args.ocnli_report_path)
    write_classification_report(tnews_result,tnews_target_name,tnews_report_output)
    write_classification_report(ocemotion_result,ocemotion_target_name,ocemotion_report_output)
    write_classification_report(ocnli_result,ocnli_target_name,ocnli_report_output)


def train(args,tnews_dataset,ocemotion_dataset,ocnli_dataset,model,optimizer):
    model.train()
    loss_avg = RunningAverage()
    data_len = len(tnews_dataset) + len(ocemotion_dataset) + len(ocnli_dataset)
    # data_len =len(ocnli_dataset)
    print('final train data size is %s'%data_len)
    tnews_sampler = RandomSampler(tnews_dataset)
    tnews_dataloader = DataLoader(tnews_dataset,sampler=tnews_sampler,batch_size=args.batch_size,collate_fn=train_collate_fn)

    ocemotion_sampler = RandomSampler(ocemotion_dataset)
    ocemotion_dataloader = DataLoader(ocemotion_dataset,sampler=ocemotion_sampler,batch_size=args.batch_size,collate_fn=train_collate_fn)

    ocnli_sampler = RandomSampler(ocnli_dataset)
    ocnli_dataloader = DataLoader(ocnli_dataset,sampler=ocnli_sampler,batch_size=args.batch_size,collate_fn=train_collate_fn)

    tnews_dataloader = cycle(tnews_dataloader)
    ocemotion_dataloader = cycle(ocemotion_dataloader)
    ocnli_dataloader = cycle(ocnli_dataloader)

    global_step = 0
    t = trange(data_len//args.batch_size)

    for i in t:
        if global_step%3==0:
            batch = next(tnews_dataloader)
            batch = tuple(t.to(args.device) for t in batch)
            input_ids,attention_mask,token_type_ids,labels,idx = batch
            logits,loss = model(input_ids,token_type_ids,attention_mask,global_step,labels=labels)
        elif global_step%3==2:
            batch = next(ocemotion_dataloader)
            batch = tuple(t.to(args.device) for t in batch)
            input_ids,attention_mask,token_type_ids,labels,idx = batch
            logits,loss = model(input_ids,token_type_ids,attention_mask,global_step,labels=labels)
        else:
            batch = next(ocnli_dataloader)
            batch = tuple(t.to(args.device) for t in batch)
            input_ids,attention_mask,token_type_ids,labels,idx = batch
            logits,loss = model(input_ids,token_type_ids,attention_mask,global_step,labels=labels)
        if args.multi_gpu and args.device_num > 1:
            loss = loss.mean()
        global_step+=1
        if global_step == 3:
            global_step=0
        model.zero_grad()
        loss.backward()
        nn.utils.clip_grad_norm_(parameters=model.parameters(),max_norm=1)
        optimizer.step()
        # scheduler.step()
        loss_avg.upgrade(loss.item())
        t.set_postfix(loss='{:05.3f}'.format(loss_avg()))

def eval_fn(model,tnews_dataset,ocemotion_dataset,ocnli_dataset,args):
    model.eval()
    tnews_sampler = SequentialSampler(tnews_dataset)
    tnews_dev_dataloader = DataLoader(tnews_dataset,sampler=tnews_sampler,batch_size=args.batch_size,collate_fn=train_collate_fn)
    ocemotion_sampler = SequentialSampler(ocemotion_dataset)
    ocemotion_dev_dataloader = DataLoader(ocemotion_dataset,sampler=ocemotion_sampler,batch_size=args.batch_size,collate_fn=train_collate_fn)
    ocnli_sampler = SequentialSampler(ocnli_dataset)
    ocnli_dev_dataloader = DataLoader(ocnli_dataset,sampler=ocnli_sampler,batch_size=args.batch_size,collate_fn=train_collate_fn)

    tnews_result = {'label':None,'predict':None}
    ocemotion_result = {'label':None,'predict':None}
    ocnli_result = {'label':None,'predict':None}
    with torch.no_grad():
        for batch in ocemotion_dev_dataloader:
            batch = tuple(t.to(args.device) for t in batch)
            input_ids,attention_mask,token_type_ids,labels,idx = batch
            labels = labels.detach().cpu().numpy()
            outputs = model(input_ids,token_type_ids,attention_mask,2)
            logits = outputs[0].detach().cpu().numpy()
            predict = np.argmax(logits,axis=1)
            if ocemotion_result['label'] is None:
                ocemotion_result['label'] = labels
                ocemotion_result['predict'] = predict
            else:
                ocemotion_result['label'] = np.append(ocemotion_result['label'],labels)
                ocemotion_result['predict'] = np.append(ocemotion_result['predict'],predict)

        for batch in tnews_dev_dataloader:
            batch = tuple(t.to(args.device) for t in batch)
            input_ids,attention_mask,token_type_ids,labels,idx = batch
            labels = labels.detach().cpu().numpy()

            outputs = model(input_ids,token_type_ids,attention_mask,0)
            logits = outputs[0].detach().cpu().numpy()
            predict = np.argmax(logits,axis=1)

            if tnews_result['label'] is None:
                tnews_result['label'] = labels
                tnews_result['predict'] = predict
            else:
                tnews_result['label'] = np.append(tnews_result['label'],labels)
                tnews_result['predict'] = np.append(tnews_result['predict'],predict)

        for batch in ocnli_dev_dataloader:
            batch = tuple(t.to(args.device) for t in batch)
            input_ids,attention_mask,token_type_ids,labels,idx = batch
            labels = labels.detach().cpu().numpy()
            outputs = model(input_ids,token_type_ids,attention_mask,1)
            logits = outputs[0].detach().cpu().numpy()
            predict = np.argmax(logits,axis=1)
            if ocnli_result['label'] is None:
                ocnli_result['label'] = labels
                ocnli_result['predict'] = predict
            else:
                ocnli_result['label'] = np.append(ocnli_result['label'],labels)
                ocnli_result['predict'] = np.append(ocnli_result['predict'],predict)

    tnews_f1_score = f1_score(tnews_result['label'],tnews_result['predict'],average='macro')
    ocemotion_f1_score = f1_score(ocemotion_result['label'],ocemotion_result['predict'],average='macro')
    ocnli_f1_score = f1_score(ocnli_result['label'],ocnli_result['predict'],average='macro')

    tnews_target_name = [str(tnews_idx2label[idx]) for idx in range(len(tnews_idx2label))]
    ocemotion_target_name = [str(ocemotion_idx2label[idx]) for idx in range(len(ocemotion_idx2label))]
    ocnli_target_name = [str(ocnli_idx2label[idx]) for idx in range(len(ocnli_idx2label))]


    print('the tnews_f1_score is %s'%(str(tnews_f1_score)))
    print('the ocemotion_f1_score is %s'%(str(ocemotion_f1_score)))
    print('the ocnli_f1_score is %s'%(str(ocnli_f1_score)))
    print('the all f1 score is %s'%(str((tnews_f1_score+ocemotion_f1_score+ocnli_f1_score)/3)))
    write_result(tnews_result,ocemotion_result,ocnli_result,tnews_target_name,ocemotion_target_name,ocnli_target_name,args)
    return (tnews_f1_score+ocemotion_f1_score+ocnli_f1_score)/3


def test(model,tnews_dataset,ocemotion_dataset,ocnli_dataset,tnews_idx2label,ocemotion_idx2label,ocnli_idx2label):
    model.eval()
    tnews_sampler = SequentialSampler(tnews_dataset)
    tnews_test_dataloader = DataLoader(tnews_dataset,sampler=tnews_sampler,batch_size=args.batch_size,collate_fn=test_collate_fn)
    ocemotion_sampler = SequentialSampler(ocemotion_dataset)
    ocemotion_test_dataloader = DataLoader(ocemotion_dataset,sampler=ocemotion_sampler,batch_size=args.batch_size,collate_fn=test_collate_fn)
    ocnli_sampler = SequentialSampler(ocnli_dataset)
    ocnli_test_dataloader = DataLoader(ocnli_dataset,sampler=ocnli_sampler,batch_size=args.batch_size,collate_fn=test_collate_fn)

    tnews_result = {'idx':[],'predict':[]}
    ocemotion_result = {'idx':[],'predict':[]}
    ocnli_result = {'idx':[],'predict':[]}

    with torch.no_grad():
        for batch in tnews_test_dataloader:
            batch = tuple(t.to(args.device) for t in batch)
            input_ids,attention_mask,token_type_ids,idx = batch
            idx = idx.detach().cpu().numpy()
            outputs = model(input_ids,token_type_ids,attention_mask,0)
            logits = outputs[0].detach().cpu().numpy()
            predict = np.argmax(logits,axis=1).tolist()
            predict = [tnews_idx2label[x] for x in predict]
            tnews_result['idx'].extend(idx)
            tnews_result['predict'].extend(predict)

        for batch in ocemotion_test_dataloader:
            batch = tuple(t.to(args.device) for t in batch)
            input_ids,attention_mask,token_type_ids,idx = batch
            idx = idx.detach().cpu().numpy()
            outputs = model(input_ids,token_type_ids,attention_mask,2)
            logits = outputs[0].detach().cpu().numpy()
            predict = np.argmax(logits,axis=1).tolist()
            predict = [ocemotion_idx2label[x] for x in predict]
            ocemotion_result['idx'].extend(idx)
            ocemotion_result['predict'].extend(predict)

        for batch in ocnli_test_dataloader:
            batch = tuple(t.to(args.device) for t in batch)
            input_ids,attention_mask,token_type_ids,idx = batch
            idx = idx.detach().cpu().numpy()
            outputs = model(input_ids,token_type_ids,attention_mask,1)
            logits = outputs[0].detach().cpu().numpy()
            predict = np.argmax(logits,axis=1).tolist()
            predict = [ocnli_idx2label[x] for x in predict]
            ocnli_result['idx'].extend(idx)
            ocnli_result['predict'].extend(predict)

    return tnews_result,ocemotion_result,ocnli_result


if __name__=='__main__':
    args = parser.parse_args()
    tnews_path = os.path.join(args.data_dir,'TNEWS_all.csv')
    ocemotion_path = os.path.join(args.data_dir,'OCEMOTION_all.csv')
    ocnli_path = os.path.join(args.data_dir,'OCNLI_all.csv')

    tokenizer = BertTokenizer.from_pretrained(args.bert_model_dir)
    # tnews_features,tnews_idx2label,ocemotion_features,ocemotion_idx2label,ocnli_features,ocnli_idx2label = get_all_data(tnews_path,ocnli_path,ocemotion_path,tokenizer,args.max_len)
    tnews_features,tnews_idx2label,tnews_dev_features,ocemotion_features,ocemotion_idx2label,ocemotion_dev_features,ocnli_features,ocnli_idx2label,ocnli_dev_features = get_train_dev_data(tnews_path,ocnli_path,ocemotion_path,tokenizer,args.max_len)

    args.tnews_label_num = len(tnews_idx2label)
    args.ocemotion_label_num = len(ocemotion_idx2label)
    args.ocnli_label_num = len(ocnli_idx2label)

    logging.info("convert tnews feature to tensor")
    tnews_dataset = convert_feature_to_tensor(tnews_features)
    logging.info("convert ocemotion feature to tensor")
    ocemotion_dataset = convert_feature_to_tensor(ocemotion_features)
    logging.info("convert ocnli feature to tensor")
    ocnli_dataset = convert_feature_to_tensor(ocnli_features)


    # logging.info("convert tnews feature to tensor")
    tnews_dev_dataset = convert_feature_to_tensor(tnews_dev_features)
    # logging.info("convert ocemotion feature to tensor")
    ocemotion_dev_dataset = convert_feature_to_tensor(ocemotion_dev_features)
    # logging.info("convert ocnli feature to tensor")
    ocnli_dev_dataset = convert_feature_to_tensor(ocnli_dev_features)

    tnwes_test_path = os.path.join(args.data_dir,'TNEWS_a.csv')
    ocemotion_test_path = os.path.join(args.data_dir,'OCEMOTION_a.csv')
    ocnli_test_path = os.path.join(args.data_dir,'OCNLI_a.csv')

    tnews_test_features,_ = trans_data(tnwes_test_path,tokenizer,args.max_len,is_test=True)
    ocemotion_test_features,_ = trans_data(ocemotion_test_path,tokenizer,args.max_len,is_test=True)
    ocnli_test_features,_ = trans_data(ocnli_test_path,tokenizer,args.max_len,is_two_sent=True,is_test=True)

    tnews_test_dataset = convert_feature_to_tensor(tnews_test_features,is_test=True)
    # logging.info("convert ocemotion feature to tensor")
    ocemotion_test_dataset = convert_feature_to_tensor(ocemotion_test_features,is_test=True)
    # logging.info("convert ocnli feature to tensor")
    ocnli_test_dataset = convert_feature_to_tensor(ocnli_test_features,is_test=True)

    device_num = torch.cuda.device_count()
    args.device_num = device_num
    print('the device number is %s'%(device_num))
    config_path = os.path.join(args.bert_model_dir,'config.json')
    config = BertConfig.from_json_file(config_path)
    model = PretrainedModel(args)

    if args.load_checkpoint and os.path.exists(args.checkpoint_path):
        print('load the model from the path of %s'%args.checkpoint_path)
        load_checkpoint(args.checkpoint_path,model)

    if args.multi_gpu and device_num > 1:
        print('Using multi gpu to train...')
        model = torch.nn.DataParallel(model)
    model.to(args.device)

    no_decay = ['bias', 'LayerNorm.bias', 'LayerNorm.weight']
    optimizer_grouped_parameters = [
        {'params':[p for n,p in model.named_parameters() if not any(nd in n for nd in no_decay)],'weight_decay':0.01},
        {'params':[p for n,p in model.named_parameters() if any(nd in n for nd in no_decay)],'weight_decay':0.0}
    ]
    logging.info("logging the model")

    tnews_predict_path = os.path.join(args.test_output,args.tnews_predict_path)
    ocemotion_predict_path = os.path.join(args.test_output,args.ocemotion_predict_path)
    ocnli_output_path = os.path.join(args.test_output,args.ocnli_predict_path)

    data_len = len(tnews_dataset) + len(ocemotion_dataset) + len(ocnli_dataset)
    num_train_optimization_steps = int(data_len / args.batch_size * args.epoches)

    optimizer = BertAdam(optimizer_grouped_parameters,lr=args.learning_rate,warmup=0.1,t_total=num_train_optimization_steps)

    # optimizer = AdamW(optimizer_grouped_parameters,lr=args.learning_rate)

    
    logging.info("starting to training ")
    best_f1 = 0
    for epoch in range(args.epoches):
        train(args,tnews_dataset,ocemotion_dataset,ocnli_dataset,model,optimizer)
        f1 = eval_fn(model,tnews_dev_dataset,ocemotion_dev_dataset,ocnli_dev_dataset,args)
        if f1 > best_f1:
            best_f1 = f1
            model_to_save = model.module if hasattr(model,'module') else model
            save_checkpoint({'epoch':epoch+1,
                                'state_dict':model_to_save.state_dict()},checkpoint=args.model_dir)
        if epoch%10==9:
            tnews_result,ocemotion_result,ocnli_result = test(model,tnews_test_dataset,ocemotion_test_dataset,ocnli_test_dataset,tnews_idx2label,ocemotion_idx2label,ocnli_idx2label)
            write_output_data(tnews_predict_path,tnews_result)
            write_output_data(ocemotion_predict_path,ocemotion_result)
            write_output_data(ocnli_output_path,ocnli_result)
    # if is_test:
    #     tnews_test_data =
    # 