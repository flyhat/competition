

# NLP中文预训练模型泛化能力挑战赛

## 项目目录

```
tianchi/
│  data_process.py
│  file_utils.py
│  model.py
│  modeling_bert.py
│  modeling_wwm.py
│  optimization.py
│  train.py
│  utils.py
├─bert
│      config.json
│      vocab.txt
│      pretrained_model
├─data
│      OCEMOTION_a.csv
│      OCEMOTION_all.csv
│      OCNLI_a.csv
│      OCNLI_all.csv
│      TNEWS_a.csv
│      TNEWS_all.csv
├─model_output
│      ocemotion_predict.json
│      ocnli_predict.json
│      tnews_predict.json
├─result_output
└─submition
```

## Requirements

```
boto3
torch==1.4.0
python==3.7.0
pandas==0.25.2
sklearn
```

## Download

- 下载Bert预训练模型存放到bert\目录下

## Quickstart

```
python train.py
```



